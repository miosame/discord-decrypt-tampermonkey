// ==UserScript==
// @name         Decrypt Discord
// @version      0.1
// @author       Mio
// @match        https://discord.com/channels/*
// @require      https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js
// ==/UserScript==

(function() {
    'use strict';
    // cheaper than MutationObserver, with same effect
    setInterval(() => {
        let messages = document.querySelectorAll(".messageContent-2qWWxC");
        for (let i = 0; i < messages.length; i++) {
            // CryptoJS does not have a sane fallback
            // in the future could sign the encrypted header and check for that
            try {
                if(messages[i].innerHTML.startsWith(";enc")) {
                    messages[i].innerHTML = CryptoJS.AES.decrypt(messages[i].innerHTML.replace(";enc",""),"f>6<`vLX<$a=4YjM2z>`76Xm6kmXJzzNtF8v!%vpr9S7VsbR%~g?^s?ETR$ec>F<mcoB#Dn=").toString(CryptoJS.enc.Utf8);
                }
            } catch (e) {}
        }
    },1000);
})();