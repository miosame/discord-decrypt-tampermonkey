## get tampermonkey

- Firefox: https://addons.mozilla.org/en-US/firefox/addon/tampermonkey/
- Chrome: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

## install script

Follow this link and confirm installation: https://gitlab.com/miosame/discord-decrypt-tampermonkey/-/raw/master/decrypt.user.js

This way the script should allow update via tampermonkey, if I have to push an update. (it won't update without your consent)

## final notes

You're ready to go, open discord web and go to a channel that has encrypted messages to see them, your messages will be encrypted by the bot automatically.
